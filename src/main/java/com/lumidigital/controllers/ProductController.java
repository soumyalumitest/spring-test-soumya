package com.lumidigital.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import com.lumidigital.service.ProductService;
import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestParam;
import java.util.Collection;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.ui.ModelMap;

@Controller
public class ProductController {

	@Resource(name="productService")
	ProductService productService;


	/**
	 *   Task -1
	 * @param model
	 * @return
	 */
	@RequestMapping(name="/product/list" ,method={RequestMethod.GET})
	public String product(Model model) {
		model.addAttribute("products", productService.getProducts());
		model.addAttribute("currency","USD");
		return "product";
	}


	/**
	 *  Task 2
	 * @param model
	 * @param currency
	 * @return
	 */
	@RequestMapping(name="/product/currency" ,method={RequestMethod.POST})
	public String currency(Model model,@RequestParam(value = "currency") final String currency) {
		model.addAttribute("currency",currency);
		return "product";
	}

	/**
	 *  Task 2
	 * @return
	 */
	@ModelAttribute("currencies")
	public Collection<String> getCurrencies()
	{
     List<String> curr=new ArrayList<String>();
     curr.add("USD");
     curr.add("EUR");
     curr.add("INR");
	 return curr;
	}
}
