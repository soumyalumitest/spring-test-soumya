package com.lumidigital.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import com.lumidigital.service.ProductService;
import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestParam;
import java.util.Collection;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.ui.ModelMap;
import com.lumidigital.model.Product;
import javax.validation.Valid;
import org.springframework.validation.BindingResult;

@Controller
public class EditController {
    @Resource(name = "productService")
    ProductService productService;

    /**
     * Task -3
     *
     * @param id
     * @param model
     * @return
     */
    @RequestMapping(value = "/product/edit/{id}", method = RequestMethod.GET)
    public String editapl(@PathVariable("id") Long id, ModelMap model) {
        model.put("product", productService.getProductForId(id));
        return "editproduct";
    }

    @RequestMapping(value = "/product/save", method = RequestMethod.POST)
    public String save(@Valid @ModelAttribute("productForm") Product prd, BindingResult result, ModelMap model) {
        {
            if (result.hasErrors()) {
                return "editproduct";
            }

            return "redirect:/product/list";
        }
    }
}