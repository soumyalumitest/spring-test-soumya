package com.lumidigital.beans;


import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.Currency;
import com.lumidigital.model.Product;
import java.util.List;
public class ProductForm {

	private String id;
	private String name;
	private String price;

	public String getPrice() {
		return price;
	}

	public void setPrice(String price) {
		this.price = price;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<Product> getProductlist() {
		return productlist;
	}

	public void setProductlist(List<Product> productlist) {
		this.productlist = productlist;
	}

	private List<Product> productlist;


	@NotNull(message = "{Id is empty}")
	@Size(min = 1, max = 50, message = "{Length exceeds}")
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}


}
