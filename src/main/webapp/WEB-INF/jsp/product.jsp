<!DOCTYPE html>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<html lang="en">
<body>
<h1>Product List</h1>
<h2> Name </h2>
<h2> Price </h2>
<c:forEach items="${products}" var="prd">
 ${prd.name}
<c:forEach items="${prd.prices}" var="price">
${price}
</c:forEach>

<td><a href="/product/edit/{prd.id}">edit</a></td>
</c:forEach>

<form action="product/currency" method="post">
<h1> Currency List </h1>
<select name="currency">
        <c:forEach items="${currencies}" var="currency">
            <option value="${currency}">${currency}</option>
        </c:forEach>
</select>
</form>

</body>
</html>